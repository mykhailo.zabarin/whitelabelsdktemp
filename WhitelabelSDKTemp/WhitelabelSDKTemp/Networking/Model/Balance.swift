//
//  Balance.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public struct Balance {
    
    public let pointsValue: Int
    public let monetaryValue: Double
}

extension Balance: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case pointsValue
        case monetaryValue
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.pointsValue = try container.decode(Int.self, forKey: .pointsValue)
        self.monetaryValue = try container.decode(Double.self, forKey: .monetaryValue)
    }
}
