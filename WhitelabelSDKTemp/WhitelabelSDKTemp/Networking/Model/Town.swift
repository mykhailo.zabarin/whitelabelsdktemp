//
//  Town.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 25.01.2022.
//

import Foundation

public struct Town {
    
    public let id: String
    public var name: String?
    public var countryCode: String?
}

extension Town: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case countryCode
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.countryCode = try? container.decode(String.self, forKey: .countryCode)
    }
}

