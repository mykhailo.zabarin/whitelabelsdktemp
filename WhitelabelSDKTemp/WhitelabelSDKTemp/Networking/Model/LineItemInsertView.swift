//
//  LineItemInsertView.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 26.01.2022.
//

import Foundation

public struct LineItemInsertView {
    
    public let id: String
    public let quantity: Double
    public let unitPrice: String
    public var unitPriceNumber: Double?
    public var description: String?
    public var groupId: String?
}

extension LineItemInsertView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case quantity
        case unitPrice
        case unitPriceNumber
        case description
        case groupId
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.id = try container.decode(String.self, forKey: .id)
        self.quantity = try container.decode(Double.self, forKey: .quantity)
        self.unitPrice = try container.decode(String.self, forKey: .unitPrice)
        self.unitPriceNumber = try? container.decode(Double.self, forKey: .unitPriceNumber)
        self.description = try? container.decode(String.self, forKey: .description)
        self.groupId = try? container.decode(String.self, forKey: .groupId)
    }
}
