//
//  CustomerCouponResultView.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 01.02.2022.
//

import Foundation

public struct CustomerCouponResultView {
    
    public let coupon: CouponResultView
    public let id: String
    public let customerId: String?
    public let usesLeft: Int
    public let favorite: Bool?
    public let barCode: String
    public let rewardId: String
    public let createdDate: String?
    public let couponId: String
    public let from: String
    public let to: String
    public let isUsed: Bool
    
    public var barcodeUrl: URL {
        return URL(string: "\(LoyaleConfig.shared.baseLoyaltyURL.absoluteString)/api/BarCode/barcode?barcode=\(barCode)")!
    }
    
    public var qrCodeUrl: URL {
        return URL(string: "\(LoyaleConfig.shared.baseLoyaltyURL.absoluteString)/api/BarCode/qrcode?qrcode=\(barCode)")!
    }
}

extension CustomerCouponResultView: Decodable {
   
    enum CodingKeys: String, CodingKey {
        case coupon
        case id
        case customerId
        case usesLeft
        case favorite
        case barCode
        case rewardId
        case createdDate
        case couponId
        case from
        case to
        case isUsed
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.coupon = try container.decode(CouponResultView.self, forKey: .coupon)
        self.id = try container.decode(String.self, forKey: .id)
        self.customerId = try? container.decode(String.self, forKey: .customerId)
        self.usesLeft = try container.decode(Int.self, forKey: .usesLeft)
        self.favorite = try? container.decode(Bool.self, forKey: .favorite)
        self.barCode = try container.decode(String.self, forKey: .barCode)
        self.rewardId = try container.decode(String.self, forKey: .rewardId)
        self.couponId = try container.decode(String.self, forKey: .couponId)
        self.isUsed = try container.decode(Bool.self, forKey: .isUsed)
        self.to = try container.decode(String.self, forKey: .to)
        self.from = try container.decode(String.self, forKey: .from)
        self.createdDate = try container.decode(String.self, forKey: .createdDate)
    }
}
