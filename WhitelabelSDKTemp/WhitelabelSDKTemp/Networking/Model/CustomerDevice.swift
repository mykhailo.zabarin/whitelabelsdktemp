//
//  CustomerDevice.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import UIKit

public struct CustomerDevice {
    
    public let id: String
    public var customerId: String?
    public var schemeId: String
    public var deviceName: String?
    public var deviceVersion: String?
    public var osName: String?
    public var osVersion: String?
    public var appName: String?
    public var appVersion: String?
    public var comments: String?
    public var firstDevice: Bool?
    public var createdBy: String?
    public var updatedBy: String?
    public var createdDate: String?
    public var updatedDate: String?
}

extension CustomerDevice {
    public  init(comments: String) {
        self.id = ""
        self.schemeId = LoyaleConfig.shared.schemeId
        self.deviceName = ""
        self.deviceVersion = "\(UIDevice.modelName)"
        self.osName = "iOs"
        self.osVersion = "\(UIDevice.current.systemVersion)"
        self.appName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? "Unknown App"
        self.appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "Unknown Version"
        self.comments = comments
    }
}

extension CustomerDevice {
    
    public func parameters() -> Parameters {
        do{
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let mappedUser = try? encoder.encode(self)
            let json = try JSONSerialization.jsonObject(with: mappedUser!, options: .allowFragments) as? [String : Any]
            return json ?? [String: Any]()
        } catch {
            print(error)
            return [String: Any]()
        }
    }
}

extension CustomerDevice: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case customerId
        case schemeId
        case deviceName
        case deviceVersion
        case osName
        case osVersion
        case appName
        case appVersion
        case comments
        case firstDevice
        case createdBy
        case updatedBy
        case createdDate
        case updatedDate
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.id = try container.decode(String.self, forKey: .id)
        self.customerId = try? container.decode(String.self, forKey: .customerId)
        self.schemeId = try container.decode(String.self, forKey: .schemeId)
        self.deviceName = try? container.decode(String.self, forKey: .deviceName)
        self.deviceVersion = try? container.decode(String.self, forKey: .deviceVersion)
        self.osName = try? container.decode(String.self, forKey: .osName)
        self.osVersion = try? container.decode(String.self, forKey: .osVersion)
        self.appName = try? container.decode(String.self, forKey: .appName)
        self.appVersion = try? container.decode(String.self, forKey: .appVersion)
        self.comments = try? container.decode(String.self, forKey: .comments)
        self.firstDevice = try? container.decode(Bool.self, forKey: .firstDevice)
        self.createdBy = try? container.decode(String.self, forKey: .createdBy)
        self.updatedBy = try? container.decode(String.self, forKey: .updatedBy)
        self.updatedDate = try? container.decode(String.self, forKey: .updatedDate)
        self.createdDate = try? container.decode(String.self, forKey: .createdDate)
    }
}

extension CustomerDevice: Encodable {
    
    public func encode(to encoder: Encoder) throws {
    
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(self.deviceName, forKey: .deviceName)
        try? container.encode(self.deviceVersion, forKey: .deviceVersion)
        try? container.encode(self.osName, forKey: .osName)
        try? container.encode(self.osVersion, forKey: .osVersion)
        try? container.encode(self.appName, forKey: .appName)
        try? container.encode(self.appVersion, forKey: .appVersion)
        try? container.encode(self.comments, forKey: .comments)
    }
}
