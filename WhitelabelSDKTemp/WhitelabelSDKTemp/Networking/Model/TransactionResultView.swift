//
//  TransactionResultView.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 25.01.2022.
//

import Foundation

public enum LoyaleTransactionType: Int, Decodable {
    
    case Purchase = 0, PositiveAdjustment, NegativeAdjustment, Redeem, Refund, TopUp, OnHold
    
    public init(from decoder: Decoder) throws {
        let value = try decoder.singleValueContainer().decode(Int.self)
        self = LoyaleTransactionType(rawValue: value) ?? .Purchase
    }
}

public struct TransactionResultView {
    
    enum CodingKeys: String, CodingKey {
        case id
        case lineItems
        case value
        case monetaryValue
        case saleCurrency
        case customerId
        case customer
        case transactionType
        case outletId
        case pointValue
        case allocatedPointValue
        case externalRefId
        case description
        case transactionDate
        case schemeId
        case couponsUsed
        case outlet
        case cachedResultantPointValue
        case isSendPointsAction
        case isSplitted
        case targetCustomer
        case sourceCustomer
        case splittedBetween
        case posId
        case posType
        case scheme
        case targetTransactionId
        case sourceTransactionId
        case groupId
        case deleted
        case createdBy
        case updatedBy
        case hidden
        case createdDate
        case updatedDate
    }
    
    public let id: String
    public let monetaryValue: Double?
    public let customerId: String
    public var customer: CustomerResultView?
    public var saleCurrency: String
    public var outletId: String?
    public var pointValue: Int
    public var externalRefId: String
    public var description: String
    public var transactionDate: String?
    public var schemeId: String
    public var outlet: OutletResultView?
    public var allocatedPointValue: Double
    public var deleted: Bool
    public var createdBy: String?
    public var createdDate: String?
    public var updatedBy: String?
    public var updatedDate: String?
    public var hidden: Bool?
    public var transactionType: LoyaleTransactionType
    public var cachedResultantPointValue: Double
    public var isSendPointsAction: Bool?
    public var isSplitted: Bool?
    public var targetCustomer: CustomerResultView?
    public var sourceCustomer: CustomerResultView?
    public var splittedBetween: [CustomerResultView]?
    public var couponsUsed: [CouponResultView]?
    public var posType: String?
    public var value: String
    public var lineItems: [LineItemInsertView]?
    public var posId: String?
    public var scheme: SchemeResultView?
    public var targetTransactionId: String?
    public var sourceTransactionId: String?
    public var groupId: String?
}
    
extension TransactionResultView: Decodable {
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.monetaryValue = try container.decode(Double.self, forKey: .monetaryValue)
        self.customerId = try container.decode(String.self, forKey: .customerId)
        self.customer = try? container.decode(CustomerResultView.self, forKey: .customerId)
        self.saleCurrency = try container.decode(String.self, forKey: .saleCurrency)
        self.pointValue = try container.decode(Int.self, forKey: .pointValue)
        self.externalRefId = try container.decode(String.self, forKey: .externalRefId)
        self.cachedResultantPointValue = try container.decode(Double.self, forKey: .cachedResultantPointValue)
        self.schemeId = try container.decode(String.self, forKey: .schemeId)
        self.value = try container.decode(String.self, forKey:  .value)
        self.couponsUsed = try? container.decode([CouponResultView].self, forKey: .couponsUsed)
        self.splittedBetween = try? container.decode([CustomerResultView].self, forKey: .splittedBetween)
        self.targetCustomer = try? container.decode(CustomerResultView.self, forKey: .targetCustomer)
        self.sourceCustomer = try? container.decode(CustomerResultView.self, forKey: .sourceCustomer)
        self.outlet = try? container.decode(OutletResultView.self, forKey: .outlet)
        self.outletId = try? container.decode(String.self, forKey: .outletId)
        self.description = try container.decode(String.self, forKey: .description)
        self.isSendPointsAction = try? container.decode(Bool.self, forKey: .isSendPointsAction)
        self.isSplitted = try? container.decode(Bool.self, forKey: .isSplitted)
        self.posId = try? container.decode(String.self, forKey: .posId)
        self.scheme = try? container.decode(SchemeResultView.self, forKey: .scheme)
        self.targetTransactionId = try? container.decode(String.self, forKey: .targetTransactionId)
        self.sourceTransactionId = try? container.decode(String.self, forKey: .sourceTransactionId)
        self.lineItems = try? container.decode([LineItemInsertView].self, forKey: .lineItems)
        self.posType = try? container.decode(String.self, forKey: .posType)
        self.transactionType = try container.decode(LoyaleTransactionType.self, forKey: .transactionType)
        self.allocatedPointValue = try container.decode(Double.self, forKey: .allocatedPointValue)
        self.deleted = try container.decode(Bool.self, forKey: .deleted)
        self.createdBy = try? container.decode(String.self, forKey: .createdBy)
        self.hidden = try? container.decode(Bool.self, forKey: .hidden)
        self.groupId = try? container.decode(String.self, forKey: .groupId)
        self.updatedBy = try? container.decode(String.self, forKey: .updatedBy)
        self.transactionDate = try container.decode(String.self, forKey: .transactionDate)
        self.createdDate  = try? container.decode(String.self, forKey: .createdDate)
        self.updatedDate  = try? container.decode(String.self, forKey: .updatedDate)
    }
}

