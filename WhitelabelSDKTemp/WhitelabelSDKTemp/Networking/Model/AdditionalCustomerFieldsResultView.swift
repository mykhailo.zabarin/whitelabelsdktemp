//
//  CustomerAdditionalField.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public struct AdditionalCustomerFieldsResultView {
    
    public let customerId: String
    public let id: String?
    public let schemeId: String?
    public let name: String
    public let key: String
    public var value: String
    public var updatedDate: String?
    public var createdDate: String?
    public let isInternal: Bool?
}

extension AdditionalCustomerFieldsResultView {
    
    public init (customerId: String, schemeId: String, name: String, key: String, value: String ) {
        self.customerId = customerId
        self.schemeId = schemeId
        self.name = name
        self.key = key
        self.value = value
        self.id = nil
        self.updatedDate = nil
        self.createdDate = nil
        self.isInternal = nil
    }
}

extension AdditionalCustomerFieldsResultView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
       case customerId
       case id
       case schemeId
       case name
       case key
       case value
       case updatedDate
       case createdDate
       case isInternal = "internal"
    }
    
    public init(from decoder: Decoder) throws {
       
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.key = try container.decode(String.self, forKey: .key)
        self.value = try container.decode(String.self, forKey: .value)
        self.customerId = try container.decode(String.self, forKey: .customerId)
        self.schemeId = try container.decode(String.self, forKey: .schemeId)
        self.isInternal = try? container.decode(Bool.self, forKey: .isInternal)
        self.updatedDate = try? container.decode(String.self, forKey: .updatedDate)
        self.createdDate = try? container.decode(String.self, forKey: .createdDate)
    }
}

extension AdditionalCustomerFieldsResultView: Encodable {
    
    public func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.customerId, forKey: .customerId)
        try? container.encode(self.id, forKey: .id)
        try container.encode(self.schemeId, forKey: .schemeId)
        try container.encode(self.key, forKey: .key)
        try container.encode(self.value, forKey: .value)
        try container.encode(self.name, forKey: .name)
    }
}
