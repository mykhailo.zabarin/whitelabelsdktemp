//
//  AlertLinked.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 04.02.2022.
//

import Foundation

public enum CustomerAlertStatus: Int, Decodable {
    
    case New = 0, Read, Dismissed
    
    public init(from decoder: Decoder) throws {
        let value = try decoder.singleValueContainer().decode(Int.self)
        self = CustomerAlertStatus(rawValue: value)  ?? .New
    }
}

public struct AlertLinked {
    
    public let id: String
    public let customer: CustomerResultView?
    public let customerId: String
    public let alert: AlertResultView
    public let alertId: String
    public let status: CustomerAlertStatus
    public let createdBy: String?
    public let updatedBy: String?
    public let createdDate: String?
    public let updatedDate: String?
}

extension AlertLinked: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case customer
        case customerId
        case alert
        case alertId
        case status
        case createdBy
        case updatedBy
        case createdDate
        case updatedDate
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.customer = try? container.decode(CustomerResultView.self, forKey: .customer)
        self.customerId = try container.decode(String.self, forKey: .customerId)
        self.alert = try container.decode(AlertResultView.self, forKey: .alert)
        self.alertId = try container.decode(String.self, forKey: .alertId)
        self.status = try container.decode(CustomerAlertStatus.self, forKey: .status)
        self.createdBy = try? container.decode(String.self, forKey: .createdBy)
        self.updatedBy = try? container.decode(String.self, forKey: .updatedBy)
        self.createdDate = try? container.decode(String.self, forKey: .createdDate)
        self.updatedDate = try? container.decode(String.self, forKey: .updatedDate)
    }
}
