//
//  OutletResultView.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public struct OutletResultView {
    
    public let id: String
    public var name: String?
    public var imageUrl: URL?
    public var franchiseId: String?
    public var coordinate: CoordinateView?
    public var address: AddressView?
    public var phoneNumber: String?
    public var openingHours: [String]?
    public var facebook: String?
    public var instagram: String?
    public var imageGallery: [String]?
    public var franchise: FranchiseResultView?
    public var hidden: Bool
}

extension OutletResultView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case imageUrl
        case franchiseId
        case coordinate
        case address
        case phoneNumber
        case openingHours
        case facebook
        case instagram
        case imageGallery
        case franchise
        case hidden
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.hidden = try container.decode(Bool.self, forKey: .hidden)
        self.franchiseId = try? container.decode(String.self, forKey: .franchiseId)
        self.name = try? container.decode(String.self, forKey: .name)
        self.coordinate = try? container.decode(CoordinateView.self, forKey: .coordinate)
        self.address = try? container.decode(AddressView.self, forKey: .address)
        self.phoneNumber = try? container.decode(String.self, forKey: .phoneNumber)
        self.openingHours = try? container.decode([String].self, forKey: .openingHours)
        self.facebook = try? container.decode(String.self, forKey: .facebook)
        self.instagram = try? container.decode(String.self, forKey: .instagram)
        self.imageGallery = try? container.decode([String].self, forKey: .imageGallery)
        self.franchise = try? container.decode(FranchiseResultView.self, forKey: .franchise)
        
        if let str = try? container.decode(String.self, forKey: .imageUrl), let url = URL(string: str) {
            self.imageUrl = url
        }
    }
}
