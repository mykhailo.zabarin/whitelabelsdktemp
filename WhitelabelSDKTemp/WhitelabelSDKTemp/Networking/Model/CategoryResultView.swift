//
//  CategoryResultView.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public struct CategoryResultView {
   
    public let id: String
    public let name: String?
    public let imageUrl: String?
}

extension CategoryResultView: Decodable {
   
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case imageUrl
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.imageUrl = try? container.decode(String.self, forKey: .imageUrl)
    }
}
