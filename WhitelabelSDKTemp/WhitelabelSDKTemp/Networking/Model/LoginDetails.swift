//
//  LoginDetails.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public struct LoginDetails {
    
    public let accessToken: String
    public let userID: String
    public let userName: String?
    public let req2FA: Bool?
}

extension LoginDetails: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case token
        case userName
        case userId
        case req2FA
    }
    
    public init(from decoder: Decoder) throws {
       
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.accessToken = try container.decode(String.self, forKey: .token)
        self.userID = try container.decode(String.self, forKey: .userId)
        self.userName = try? container.decode(String.self, forKey: .userName)
        self.req2FA = try? container.decode(Bool.self, forKey: .req2FA)
    }
}
