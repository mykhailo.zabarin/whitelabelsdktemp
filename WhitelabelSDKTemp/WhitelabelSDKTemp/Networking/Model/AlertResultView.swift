//
//  AlertResultView.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 27.01.2022.
//

import Foundation

public struct AlertResultView {
    
    public let id: String
    public let title: String?
    public let schemeId: String
    public let subTitle: String?
    public let text: String?
    public let imageUrl: String?
    public let thumbnailUrl: String?
    public let updatedDate: String?
    public let createdDate: String?
    public let deepLinkId: String?
    public let deepLinkType: String?
    public let isMarketingMaterial: Bool
}

extension AlertResultView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case schemeId
        case subTitle
        case text
        case imageUrl
        case thumbnailUrl
        case updatedDate
        case createdDate
        case deepLinkId
        case deepLinkType
        case isMarketingMaterial
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.id = try container.decode(String.self, forKey: .id)
        self.title = try? container.decode(String.self, forKey: .title)
        self.schemeId = try container.decode(String.self, forKey: .schemeId)
        self.subTitle = try? container.decode(String.self, forKey: .subTitle)
        self.text = try? container.decode(String.self, forKey: .text)
        self.imageUrl = try? container.decode(String.self, forKey: .imageUrl)
        self.thumbnailUrl = try? container.decode(String.self, forKey: .thumbnailUrl)
        self.updatedDate = try? container.decode(String.self, forKey: .updatedDate)
        self.createdDate = try? container.decode(String.self, forKey: .createdDate)
        self.deepLinkId = try? container.decode(String.self, forKey: .deepLinkId)
        self.deepLinkType = try? container.decode(String.self, forKey: .deepLinkType)
        self.isMarketingMaterial = try container.decode(Bool.self, forKey: .isMarketingMaterial)
    }
}
