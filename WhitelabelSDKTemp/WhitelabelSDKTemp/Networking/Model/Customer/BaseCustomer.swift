//
//  BaseCustomer.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public enum Gender: Int, Decodable {
    case Male = 0, Female, NonBinary, Undefined
    
    public var stringValue: String {
        switch self {
        case .Male:
            return "Male"
        case .Female:
            return "Female"
        case .NonBinary:
            return "Non-binary"
        case .Undefined:
            return "Undefined"
        }
    }
    
    public init(from decoder: Decoder) throws {
        let value = try decoder.singleValueContainer().decode(Int.self)
        self = Gender(rawValue: value)  ?? .NonBinary
    }
}

public class BaseCustomer: Codable {

    public var firstName: String?
    public var lastName: String?
    public var dateOfBirth: String?
    public var email: String?
    public var areaCode: String?
    public var mobileNumber: String?
    public var gender: Gender?
    public var town: String?
    public var state: String?
    public var postCode: String?
    public var addressLine1: String?
    public var addressLine2: String?
    public var country: String?
    public var marketingSub: Bool?
    public var profileImageUrl: String?
    public var externalRefId: String?
    
    public init() {}
    
    enum CodingKeys: String, CodingKey {
        case firstName
        case lastName
        case dob
        case email
        case areaCode
        case mobileNumber
        case gender
        case town
        case state
        case postCode
        case addressLine1
        case addressLine2
        case country
        case marketingSub
        case profileImageUrl
        case externalRefId
    }
    
    required public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.firstName = try? container.decode(String.self, forKey: .firstName)
        self.lastName = try? container.decode(String.self, forKey: .lastName)
        self.email = try? container.decode(String.self, forKey: .email)
        self.gender = try? container.decode(Gender.self, forKey: .mobileNumber)
        self.mobileNumber = try? container.decode(String.self, forKey: .mobileNumber)
        self.town = try? container.decode(String.self, forKey: .town)
        self.state = try? container.decode(String.self, forKey: .state)
        self.postCode = try? container.decode(String.self, forKey: .postCode)
        self.addressLine1 = try? container.decode(String.self, forKey: .addressLine1)
        self.addressLine2 = try? container.decode(String.self, forKey: .addressLine2)
        self.country = try? container.decode(String.self, forKey: .country)
        self.marketingSub = try? container.decode(Bool.self, forKey: .marketingSub)
        self.profileImageUrl = try? container.decode(String.self, forKey: .profileImageUrl)
        self.externalRefId = try? container.decode(String.self, forKey: .externalRefId)
        self.dateOfBirth = try? container.decode(String.self, forKey: .externalRefId)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
                
        try container.encode(self.firstName, forKey: .firstName)
        try container.encode(self.lastName, forKey: .lastName)
        try container.encode(self.email, forKey: .email)
        try container.encode(self.areaCode, forKey: .areaCode)
        try container.encode(self.mobileNumber, forKey: .mobileNumber)
        try? container.encode(self.gender?.rawValue, forKey: .gender)
        try? container.encode(self.marketingSub, forKey: .marketingSub)
        try? container.encode(self.profileImageUrl, forKey: .profileImageUrl)
        try? container.encode(self.addressLine1, forKey: .addressLine1)
        try? container.encode(self.addressLine2, forKey: .addressLine2)
        try? container.encode(self.town, forKey: .town)
        try? container.encode(self.state, forKey: .state)
        try? container.encode(self.postCode, forKey: .postCode)
        try? container.encode(self.country, forKey: .country)
        try? container.encode(self.externalRefId, forKey: .externalRefId)
    }
}
