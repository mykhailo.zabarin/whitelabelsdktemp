//
//  TierResultView.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 27.01.2022.
//

import Foundation

public struct TierResultView {
    
    public let id: String
    public let schemeId: String
    public let name: String?
    public let isDefault: Bool
    public let expiryPeriod: Int
    public let upgradePeriod: Int
    public let guaranteedPeriod: Int
    public let updatedDate: String?
    public let createdDate: String?
    public let levelsUpgradable: Bool
    public let hidden: Bool
    public let levels: [LevelResultView]?
}

extension TierResultView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case schemeId
        case name
        case isDefault
        case expiryPeriod
        case upgradePeriod
        case guaranteedPeriod
        case updatedDate
        case createdDate
        case levelsUpgradable
        case hidden
        case levels
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.id = try container.decode(String.self, forKey: .id)
        self.schemeId = try container.decode(String.self, forKey: .schemeId)
        self.name = try? container.decode(String.self, forKey: .name)
        self.isDefault = try container.decode(Bool.self, forKey: .isDefault)
        self.expiryPeriod = try container.decode(Int.self, forKey: .expiryPeriod)
        self.upgradePeriod = try container.decode(Int.self, forKey: .upgradePeriod)
        self.guaranteedPeriod = try container.decode(Int.self, forKey: .guaranteedPeriod)
        self.levelsUpgradable = try container.decode(Bool.self, forKey: .levelsUpgradable)
        self.hidden = try container.decode(Bool.self, forKey: .hidden)
        self.levels = try? container.decode([LevelResultView].self, forKey: .levels)
        self.updatedDate = try? container.decode(String.self, forKey: .updatedDate)
        self.createdDate = try? container.decode(String.self, forKey: .createdDate)
    }
}
