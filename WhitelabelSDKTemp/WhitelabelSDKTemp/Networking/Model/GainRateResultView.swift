//
//  GainRateResultView.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 27.01.2022.
//

import Foundation

public enum Rounding: Int, Decodable {
    
    case Nearest = 0, Up, Down, None
    
    public init(from decoder: Decoder) throws {
        let value = try decoder.singleValueContainer().decode(Int.self)
        self = Rounding(rawValue: value) ?? .None
    }
}

public struct GainRateResultView {
    
    public let gainRate: Double
    public let rounding: Rounding
}

extension GainRateResultView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case gainRate
        case rounding
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.gainRate = try container.decode(Double.self, forKey: .gainRate)
        self.rounding = try container.decode(Rounding.self, forKey: .rounding)
    }
}

extension GainRateResultView {
    
    public func rateValueFor(_ amount: Double) -> Double {
        switch rounding {
        case .Nearest:
            return amount.rounded() * gainRate
        case .Up:
            return amount.rounded(.up) * gainRate
        case .Down:
            return amount.rounded(.down) * gainRate
        case .None:
            return amount * gainRate
        }
    }
}
