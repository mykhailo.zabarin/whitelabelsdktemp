//
//  Currency.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 25.01.2022.
//

import Foundation

public struct Currency {

    public let name: String
    public let code: String
    public let symbol: String
}

extension Currency: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case name
        case code
        case symbol
    }
    
    public init(from decoder: Decoder) throws {
       
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try container.decode(String.self, forKey: .name)
        self.code = try container.decode(String.self, forKey: .code)
        self.symbol = try container.decode(String.self, forKey: .symbol)
    }
}
