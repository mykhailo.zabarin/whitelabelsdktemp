//
//  SchemeResultView.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public struct SchemeResultView {

    public let id: String
    public var name: String?
    public var currency: String?
    public var description: String?
    public var website: URL?
    public var color: String?
    public var imageUrl: URL?
    public let pointRedemptionPerCurrency: Double
    public var updatedDate: String?
    public var createdDate: String?
    public let inLoyale: Bool
    public let hidden: Bool
    public var connectionString: String = ""
    public var rounding: Int = 0
    public var subscriptionType: Int? = 0
    public var franchises: [FranchiseResultView]?
    public var status: Int
    public var plan: PlanView?
    public var planId: String
    public var setupCompleted: Bool
    public var requireTwoFactorAuth: Bool
}

extension SchemeResultView: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case currency
        case description
        case website
        case color
        case imageUrl
        case pointRedemptionPerCurrency
        case updatedDate
        case createdDate
        case inLoyale
        case hidden
        case rounding
        case subscriptionType
        case franchises
        case status
        case plan
        case planId
        case setupCompleted
        case requireTwoFactorAuth
    }
    
    public init(from decoder: Decoder) throws {
     
        let container = try decoder.container(keyedBy: CodingKeys.self)
     
        self.id = try container.decode(String.self, forKey: .id)
        self.pointRedemptionPerCurrency = try container.decode(Double.self, forKey: .pointRedemptionPerCurrency)
        self.inLoyale = try container.decode(Bool.self, forKey: .inLoyale)
        self.hidden = try container.decode(Bool.self, forKey: .hidden)
        self.rounding = try container.decode(Int.self, forKey: .rounding)
        self.name = try? container.decode(String.self, forKey: .name)
        self.currency = try? container.decode(String.self, forKey: .currency)
        self.color = try? container.decode(String.self, forKey: .color)
        self.description = try? container.decode(String.self, forKey: .description)
        self.subscriptionType = try? container.decode(Int.self, forKey: .subscriptionType)
        self.franchises = try? container.decode([FranchiseResultView].self, forKey:  .franchises)
        self.status = try container.decode(Int.self, forKey:  .status)
        self.plan = try? container.decode(PlanView.self, forKey:  .plan)
        self.planId = try container.decode(String.self, forKey:  .planId)
        self.setupCompleted = try container.decode(Bool.self, forKey:  .setupCompleted)
        self.requireTwoFactorAuth = try container.decode(Bool.self, forKey:  .requireTwoFactorAuth)
        self.updatedDate = try? container.decode(String.self, forKey: .updatedDate)
        self.createdDate = try? container.decode(String.self, forKey: .createdDate)
        
        if let str = try? container.decode(String.self, forKey: .website), let url = URL(string: str) {
            self.website = url
        }
        
        if let str = try? container.decode(String.self, forKey: .imageUrl), let url = URL(string: str) {
            self.imageUrl = url
        }

        
    }
}
