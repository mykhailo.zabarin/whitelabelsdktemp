//
//  HelpersEndpoint.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public enum HelpersEndpoint {
    case getTowns(countryCode: String)
    case getCountries
    case getCurrency(filters: String)
    case getMobileCodes
}

extension HelpersEndpoint: EndPointType {
    
    public var loyaleToken: String {
        return ""
    }
    
    public var schemeId: String {
        return ""
    }
    
    
    public var baseURL: URL {
        return LoyaleConfig.shared.baseLoyaltyURL
    }
    
    public var path: String {
        switch self {
        case .getTowns:
            return "api/Helpers/Towns"
        case .getCountries:
            return "api/Helpers/Countries"
        case .getCurrency:
            return "api/Helpers/Currency"
        case .getMobileCodes:
            return "api/Helpers/MobileCode"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .getTowns, .getCountries, .getCurrency, .getMobileCodes:
            return .get
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .getTowns(countryCode: let countryCode):
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: ["countryCode": "\(countryCode)"], additionHeaders: headers)
        case .getCountries:
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: headers)
        case .getCurrency(let filters):
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: ["Filters": filters], additionHeaders: headers)
        case .getMobileCodes:
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: headers)
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        default:
            return nil
        }
    }
}
