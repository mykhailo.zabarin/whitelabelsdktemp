//
//  TransactionEndpoint.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public enum TransactionEndpoint {
    case getTransactions(filters: String?, sorts: String?, page: Int?, pageSize: Int?)
    case getTransactionById(_ id: String)
    case getGainRate(outletId: String)
}

extension TransactionEndpoint: EndPointType {
    
    public var loyaleToken: String {
        return "Bearer \(LoyaleConfig.shared.token)"
    }
    
    public var schemeId: String {
        return LoyaleConfig.shared.schemeId
    }
    
    public var baseURL: URL {
        return LoyaleConfig.shared.baseLoyaltyURL
    }
    
    public var path: String {
        switch self {
        case .getTransactions:
            return "api/Transaction"
        case .getTransactionById(let transactionId):
            return "api/Transaction/\(transactionId)"
        case .getGainRate(let outletId):
            return "api/Transaction/GainRate/\(outletId)"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .getTransactions, .getTransactionById, .getGainRate:
            return .get
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .getTransactions(let filters, let sorts, let page, let pageSize):
            var params = Parameters()
            filters != nil ? params["Filters"] = filters! : nil
            sorts != nil ? params["Sorts"] = sorts! : nil
            page != nil ? params["Page"] = page : nil
            pageSize != nil ? params["PageSize"] = pageSize! : nil
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: params , additionHeaders: headers)
        case .getTransactionById, .getGainRate:
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: headers)
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Scheme": schemeId, "Authorization": loyaleToken]
        }
    }
}
