//
//  AccountEndpoint.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public enum AccountEndpoint {
    case authUser(email: String, password: String, device: CustomerDevice)
    case forgotPassword(email: String)
    case changePassword(userId: String, currentPassword: String, newPassword: String)
}

extension AccountEndpoint: EndPointType {
    
    public var loyaleToken: String {
        return "Bearer \(LoyaleConfig.shared.token)"
    }
    
    public var schemeId: String {
        return LoyaleConfig.shared.schemeId
    }
    
    public var baseURL: URL {
        return LoyaleConfig.shared.baseLoyaltyURL
    }
    
    public var path: String {
        switch self {
        case .authUser:
            return "api/Token"
        case .forgotPassword:
            return "api/Account/ForgotPassword"
        case .changePassword:
            return "api/Account/ChangePassword"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .authUser:
            return .post
        case .forgotPassword:
            return .post
        case .changePassword:
            return .post
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .authUser(let username, let password, let device):
            return .requestParametersAndHeaders(bodyParameters: ["email": username, "password": password, "device": device.parameters()], bodyEncoding: .jsonEncoding, urlParameters: nil, additionHeaders: headers)
        case .forgotPassword(let email):
            return .requestParametersAndHeaders(bodyParameters: ["email": email], bodyEncoding: .jsonEncoding, urlParameters: nil, additionHeaders: headers)
        case .changePassword(let id, let currentPassword, let newPassword):
            return .requestParametersAndHeaders(bodyParameters: ["id": id, "currentPassword": currentPassword, "newPassword": newPassword], bodyEncoding: .jsonEncoding, urlParameters: nil, additionHeaders: headers)
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        case .authUser, .forgotPassword:
            return ["Scheme": schemeId]
        case .changePassword:
            return ["Scheme": schemeId, "Authorization": loyaleToken]
        }
    }
}
