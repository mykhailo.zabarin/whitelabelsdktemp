//
//  GroupEndpoint.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public enum GroupEndpoint {
    case getTiers(filters: String?, sorts: String?, page: Int?, pageSize: Int?)
    case getTierById(_ id: String)
}

extension GroupEndpoint: EndPointType {
   
    public var loyaleToken: String {
        return "Bearer \(LoyaleConfig.shared.token)"
    }
    
    public var schemeId: String {
        return LoyaleConfig.shared.schemeId
    }
    
    public var baseURL: URL {
        return LoyaleConfig.shared.baseLoyaltyURL
    }
    
    public var path: String {
        switch self {
        case .getTiers:
            return "api/Tier"
        case .getTierById(let tierId):
            return "api/Tier/\(tierId)"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .getTiers:
            return .get
        case .getTierById:
            return .get
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .getTiers(let filters, let sorts, let page, let pageSize):
            var params = Parameters()
            filters != nil ? params["Filters"] = filters! : nil
            sorts != nil ? params["Sorts"] = sorts! : nil
            page != nil ? params["Page"] = page : nil
            pageSize != nil ? params["PageSize"] = pageSize! : nil
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: params , additionHeaders: headers)
        case .getTierById:
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: headers)
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Scheme": schemeId, "Authorization": loyaleToken]
        }
    }
}
