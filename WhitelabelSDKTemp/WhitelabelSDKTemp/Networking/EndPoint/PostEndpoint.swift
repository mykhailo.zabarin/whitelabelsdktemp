//
//  PostEndpoint.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public enum PostEndpoint {
    case getAlerts(filters: String?, sorts: String?, page: Int?, pageSize: Int?)
    case getAlertById(_ id: String)
}

extension PostEndpoint: EndPointType {
    
    public var loyaleToken: String {
        return "Bearer \(LoyaleConfig.shared.token)"
    }
    
    public var schemeId: String {
        return LoyaleConfig.shared.schemeId
    }
    
    public var baseURL: URL {
        return LoyaleConfig.shared.baseLoyaltyURL
    }
    
    public var path: String {
        switch self {
        case .getAlerts:
            return "api/AlertsLinked"
        case .getAlertById(let outletId):
            return "api/AlertsLinked/\(outletId)"
        }
    }
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .getAlerts:
            return .get
        case .getAlertById:
            return .get
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .getAlerts(let filters, let sorts, let page, let pageSize):
            var params = Parameters()
            filters != nil ? params["Filters"] = filters! : nil
            sorts != nil ? params["Sorts"] = sorts! : nil
            page != nil ? params["Page"] = page : nil
            pageSize != nil ? params["PageSize"] = pageSize! : nil
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: params , additionHeaders: headers)
        case .getAlertById:
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: headers)
        }
    }
    
    public var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Scheme": schemeId, "Authorization": loyaleToken]
        }
    }
}

