//
//  AccountRouter.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public class AccountRouter {
    
    private let router = Router<AccountEndpoint>()
    
    public func authUser(username: String, password: String, device: CustomerDevice, completion: @escaping (_ model: LoginDetails?, _ error: String?)->()) {
        router.getData(.authUser(email: username, password: password, device: device)) { (model: LoginDetails?, error: String?) in
            completion(model, error)
        }
    }
    
    public func forgotPassword(email: String, completion: @escaping (Bool, String) -> ()) {
        router.request(.forgotPassword(email: email)) { data, response, error in
            if let response = response as? HTTPURLResponse {
                let result = self.router.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else { completion(true, "Check your email"); return }
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        if let json = jsonData as? [String: Any], let str = json["message"] as? String {
                            completion(false, str);
                        } else {
                            completion(true, "Check your email");
                        }
                    } catch {
                        print(error)
                        completion(false, NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    completion(false, self.router.errorString(fromData: data) ?? networkFailureError)
                }
            }
        }
    }
    
    public func changePassword(userId: String, currentPassword: String, newPassword: String, completion: @escaping (Bool, String) -> ()) {
        router.request(.changePassword(userId: userId, currentPassword: currentPassword, newPassword: newPassword)) { data, response, error in
            if let response = response as? HTTPURLResponse {
                let result = self.router.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else { completion(true, "Success"); return }
                    do {
                        let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        if let json = jsonData as? [String: Any], let str = json["message"] as? String {
                            completion(true, str);
                        } else {
                            completion(true, "Check your email");
                        }
                    } catch {
                        completion(false, NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    completion(false, self.router.errorString(fromData: data) ?? networkFailureError)
                }
            }
        }
    }
}

