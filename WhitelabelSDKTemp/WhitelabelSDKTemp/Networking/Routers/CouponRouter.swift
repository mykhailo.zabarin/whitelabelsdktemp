//
//  CouponRouter.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 27.01.2022.
//

import Foundation

public class CouponRouter {
    
    private let router = Router<CouponEndpoint>()
    
    public func getCouponsLinked(filteredBy filters: String?, sorts: String?, page: Int?, pageSize: Int?, completion: @escaping (_ model: [CustomerCouponResultView]?, _ error: String?)->()) {
        router.getData(.getCouponsLinked(filters: filters, sorts: sorts, page: page, pageSize: pageSize)) { (model: [CustomerCouponResultView]?, error: String?) in
            completion(model, error)
        }
    }
    
    public func getCouponLinked(byId id: String, completion: @escaping (_ model: CouponResultView?, _ error: String?)->()) {
        router.getData(.getCouponLinkedId(id)) { (model: CouponResultView?, error: String?) in
            completion(model, error)
        }
    }
}
