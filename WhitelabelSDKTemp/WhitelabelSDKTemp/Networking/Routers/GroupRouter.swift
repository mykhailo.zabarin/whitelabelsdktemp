//
//  GroupRouter.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 27.01.2022.
//

import Foundation

public class GroupRouter {
    
    private let router = Router<GroupEndpoint>()
    
    public func getTiers(filteredBy filters: String?, sorts: String?, page: Int?, pageSize: Int?, completion: @escaping (_ model: [TierResultView]?, _ error: String?)->()) {
        router.getData(.getTiers(filters: filters, sorts: sorts, page: page, pageSize: pageSize)) { (model: [TierResultView]?, error: String?) in
            completion(model, error)
        }
    }
    
    public func getTier(byId id: String, completion: @escaping (_ model: TierResultView?, _ error: String?)->()) {
        router.getData(.getTierById(id)) { (model: TierResultView?, error: String?) in
            completion(model, error)
        }
    }
}
