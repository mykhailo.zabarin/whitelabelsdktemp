//
//  TransactionRouter.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 25.01.2022.
//

import Foundation

public class TransactionRouter {
    
    private let router = Router<TransactionEndpoint>()
    
    public func getTransactions(filteredBy filters: String?, sorts: String?, page: Int?, pageSize: Int?, completion: @escaping (_ model: [TransactionResultView]?, _ error: String?)->()) {
        router.getData(.getTransactions(filters: filters, sorts: sorts, page: page, pageSize: pageSize)) { (model: [TransactionResultView]?, error: String?) in
            completion(model, error)
        }
    }

    public func getTransaction(byId id: String, completion: @escaping (_ model: TransactionResultView?, _ error: String?)->()) {
        router.getData(.getTransactionById(id)) { (model: TransactionResultView?, error: String?) in
            completion(model, error)
        }
    }
    
    public func getGainRate(forOutlet outletId: String, completion: @escaping (_ model: GainRateResultView?, _ error: String?)->()) {
        router.getData(.getGainRate(outletId: outletId)) { (model: GainRateResultView?, error: String?) in
            completion(model, error)
        }
    }
}
