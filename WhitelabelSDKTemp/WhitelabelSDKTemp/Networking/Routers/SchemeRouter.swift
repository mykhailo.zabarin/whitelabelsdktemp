//
//  SchemeRouter.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 21.01.2022.
//

import Foundation

public class SchemeRouter {
    
    private let router = Router<SchemeEndpoint>()
    
    public func getScheme(byId id: String, completion: @escaping (_ model: SchemeResultView?, _ error: String?)->()) {
        router.getData(.getSchemeById(id)) { (model: SchemeResultView?, error: String?) in
            completion(model, error)
        }
    }
}
