//
//  Other.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 27.01.2022.
//

import Foundation

public class OtherRouter {
    
    private let router = Router<OtherEndpoint>()
    
    public func postMessage(_ message: Message, completion: @escaping (_ success: Bool, _ error: String?)->()) {
        router.simpleRequest(.postMessage(message: message)) { success, error in
            completion(success, error)
        }
    }
}
