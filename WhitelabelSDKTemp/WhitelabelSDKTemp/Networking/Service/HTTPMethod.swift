//
//  HTTPMethod.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}
