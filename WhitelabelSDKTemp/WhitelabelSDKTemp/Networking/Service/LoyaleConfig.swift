//
//  Config.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

public enum NetworkEnvironment {
    case production
    case staging
}

public struct ConfigConfiguration {

    private let stagingBaseUrl: String = "https://api.staging.loyale.io"
    private let productionBaseUrl: String = "https://api.loyale.io"
    
    var environment: NetworkEnvironment
    var schemeId: String
    
    var baseLoyaltyURL: URL {
        switch environment {
        case .production:
            return URL(string: productionBaseUrl)!
        case .staging:
            return URL(string: stagingBaseUrl)!
        }
    }
    
    public init(environment: NetworkEnvironment, schemeId: String) {
        self.environment = environment
        self.schemeId = schemeId
    }
}

public class LoyaleConfig {

    public static let shared = LoyaleConfig()
    private static var config: ConfigConfiguration?

    public static func setup(_ config: ConfigConfiguration) {
        LoyaleConfig.config = config
    }

    private init() {
        guard let config = LoyaleConfig.config else { fatalError("Error - you must call setup before accessing MySingleton.shared") }
        self.baseLoyaltyURL = config.baseLoyaltyURL
        self.environment = config.environment
        self.schemeId = config.schemeId
    }
    
    public var environment: NetworkEnvironment
    public var baseLoyaltyURL: URL
    public var schemeId: String
    public var token: String = "Loylae API token required"
}
