//
//  Router.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation
import UIKit

public typealias NetworkRouterCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()

public protocol NetworkRouter: AnyObject {
    associatedtype EndPoint: EndPointType
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
    func cancel()
}

public class Router<EndPoint: EndPointType>: NetworkRouter {
    
    private var task: URLSessionTask?
    
    func simpleRequest(_ route: EndPoint, completion: @escaping ( _ succes: Bool,_ error: String?)->()) {
        request(route) { data, response, error in
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    completion(true, nil)
                case .failure(let networkFailureError):
                    completion(false, self.errorString(fromData: data) ?? networkFailureError)
                }
            }
        }
    }
    
    func getData<T: Decodable>(_ route: EndPoint, completion: @escaping ( _ dataModel: T?,_ error: String?)->()) {
        request(route) { data, response, error in
            if error != nil {
                completion(nil, "Please check your network connection.")
            }
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else { completion(nil, NetworkResponse.noData.rawValue); return }
                    do {
                        let model = try JSONDecoder().decode(T.self, from: responseData)
                        completion(model, nil)
                    } catch {
                        print(error)
                        completion(nil, NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    completion(nil, self.errorString(fromData: data) ?? networkFailureError)
                }
            }
        }
    }
    
    func uploadImage(_ route: EndPoint, boundary: String, image: UIImage, completion: @escaping (_ success: Bool, _ error: String?)->()) {
        do {
            let request = try self.buildRequest(from: route)
            let data = convertFileData(fieldName: "file", fileName: "blob",  mimeType: "image/png", fileData: image.pngData()!, using: boundary)
            
            URLSession.shared.uploadTask(with: request, from: data){ responseData, response, error in
                DispatchQueue.main.async {
                    if error == nil {
                        completion(true, nil)
                    } else {
                        completion(false, error?.localizedDescription ?? "Error")
                    }
                }
            }.resume()
        } catch {
            completion(false, error.localizedDescription)
        }
    }
    
    public func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        let session = URLSession.shared
        do {
            let request = try self.buildRequest(from: route)
            print(request.curlString)
            task = session.dataTask(with: request, completionHandler: { data, response, error in
                DispatchQueue.main.async {
                    completion(data, response, error)
                }
            })
        } catch {
            completion(nil, nil, error)
        }
        self.task?.resume()
    }
    
    public func cancel() {
        self.task?.cancel()
    }
    
    public func errorString(fromData data: Data?) -> String? {
        if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any], let errorArr = json["errors"] as? [String], errorArr.count > 0 {
            let errorString = errorArr.joined(separator: ", ")
            return errorString
        }
        return nil
    }
    
    internal func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdated.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
    
    fileprivate func buildRequest(from route: EndPoint) throws -> URLRequest {
        var request = URLRequest(url: route.baseURL.appendingPathComponent(route.path), cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
        request.httpMethod = route.httpMethod.rawValue
        do {
            switch route.task {
            case .request:
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            case .requestParameters(let bodyParameters, let bodyEncoding,  let urlParameters):
                try self.configureParameters(bodyParameters: bodyParameters,  bodyEncoding: bodyEncoding, urlParameters: urlParameters, bodyData: nil, request: &request)
            case .requestParametersAndHeaders(let bodyParameters,  let bodyEncoding, let urlParameters, let additionalHeaders):
                self.addAdditionalHeaders(additionalHeaders, request: &request)
                try self.configureParameters(bodyParameters: bodyParameters, bodyEncoding: bodyEncoding, urlParameters: urlParameters, bodyData: nil, request: &request)
            }
            return request
        } catch {
            throw error
        }
    }
    
    fileprivate func configureParameters(bodyParameters: Parameters?, bodyEncoding: ParameterEncoding, urlParameters: Parameters?, bodyData: Data?, request: inout URLRequest) throws {
        do {
            try bodyEncoding.encode(urlRequest: &request, bodyParameters: bodyParameters, urlParameters: urlParameters, bodyData: bodyData)
        } catch {
            throw error
        }
    }
    
    fileprivate func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
    
    fileprivate func convertFileData(fieldName: String, fileName: String, mimeType: String, fileData: Data, using boundary: String) -> Data {
        let data = NSMutableData()
        data.appendString("--\(boundary)\r\n")
        data.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n")
        data.appendString("Content-Type: \(mimeType)\r\n\r\n")
        data.append(fileData)
        data.appendString("\r\n")
        data.appendString("--\(boundary)--\r\n")
        return data as Data
    }
    
}
