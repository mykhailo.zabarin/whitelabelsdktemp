//
//  LoyaleManager.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 20.01.2022.
//

import Foundation

enum NetworkResponse:String {
    case success
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

enum Result<String>{
    case success
    case failure(String)
}

public struct LoyaleManager {

    public var account = AccountRouter()
    public var coupon = CouponRouter()
    public var customer = CustomerRouter()
    public var franchise = FranchiseRouter()
    public var group = GroupRouter()
    public var helpers = HelpersRouter()
    public var level = LevelRouter()
    public var other = OtherRouter()
    public var outlet = OutletRouter()
    public var pointBalance = PointBalanceRouter()
    public var post = PostRouter()
    public var scheme = SchemeRouter()
    public var transaction = TransactionRouter()
    
    
    public init() {}
    
}
