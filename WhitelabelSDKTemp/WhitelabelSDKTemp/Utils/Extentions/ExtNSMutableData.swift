//
//  ExtNSMutableData.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 08.04.2022.
//

import Foundation

extension NSMutableData {
  func appendString(_ string: String) {
    if let data = string.data(using: .utf8) {
      self.append(data)
    }
  }
}
