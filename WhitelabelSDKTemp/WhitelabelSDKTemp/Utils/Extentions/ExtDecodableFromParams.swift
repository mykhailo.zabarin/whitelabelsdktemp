//
//  ExtDecodableFromParams.swift
//  WhitelabelSDKTemp
//
//  Created by Mykhailo Zabarin on 03.02.2022.
//

import Foundation

public extension Decodable {
    init?(from json: [String: Any]?) throws {
        guard let aJson = json, let data = try? JSONSerialization.data(withJSONObject: aJson, options: .prettyPrinted) else { return nil }
        guard let value = try? JSONDecoder().decode(Self.self, from: data) else { return nil }
        self = value
    }
}
