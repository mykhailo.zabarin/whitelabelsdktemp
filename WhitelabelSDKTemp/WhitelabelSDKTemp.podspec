
Pod::Spec.new do |s|

s.platform = :ios
s.ios.deployment_target = '14.0'
s.name = "WhitelabelSDKTemp"
s.summary = "WhitelabelSDKTemp lets a user use the base API calls"
s.requires_arc = true

s.version = "17"

s.license = { :type => "MIT", :file => "WhitelabelSDKTemp/LICENSE.md" }

s.author = { "Mykhailo Zabarin" => "mykhailo.zabarin@gmail.com" }

s.homepage = "https://gitlab.com/mykhailo.zabarin/whitelabelsdktemp"

s.source = { :git => "https://gitlab.com/mykhailo.zabarin/whitelabelsdktemp.git", 
             :tag => "#{s.version}" }

s.source_files = "WhitelabelSDKTemp/**/*.{swift}"

s.swift_version = "5.0"

spec.source       = { :git => "https://gitlab.com/mykhailo.zabarin/whitelabelsdktemppodspecs.git", :tag => "#{spec.version}" }

end